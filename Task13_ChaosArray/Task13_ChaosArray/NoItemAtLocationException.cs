﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13_ChaosArray
{
    class NoItemAtLocationException : Exception
    {

        public NoItemAtLocationException() { }

        public NoItemAtLocationException(string message) : base(message)
        {

        }

        public NoItemAtLocationException(string message, Exception inner) : base(message, inner)
        {

        }

    }
}
