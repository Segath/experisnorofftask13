﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13_ChaosArray
{
    class PlaceTakenException : Exception
    {

        public PlaceTakenException() {}

        public PlaceTakenException(string message) : base(message)
        {

        }

        public PlaceTakenException(string message, Exception inner) : base(message, inner)
        {

        }

    }
}
