﻿using System;

namespace Task13_ChaosArray
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<string> csArray = new ChaosArray<string>(10);
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    csArray.Add(i.ToString());
                }
                catch (PlaceTakenException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            for (int i = 0; i < 3; i++)
            {
                try
                {
                    csArray.Get();
                }
                catch (NoItemAtLocationException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
