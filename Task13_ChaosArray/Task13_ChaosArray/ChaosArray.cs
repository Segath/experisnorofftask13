﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task13_ChaosArray
{
    class ChaosArray<T>
    {

        private T[] array;
        private Random rnd;

        public ChaosArray(int size = 5)
        {
            rnd = new Random();
            array = new T[size];
        }

        public void Add(T item)
        {
            int index = rnd.Next(0, array.Length - 1);
            if(array[index] == default)
            {
                array[index] = item;
            }
            else
            {
                throw new PlaceTakenException($"Tried to place an item at position {index}, " +
                    $"but it is already taken");
            }
        }

        public T Get()
        {
            int index = rnd.Next(0, array.Length - 1);
            T item = array[index];
            if(item == default)
            {
                throw new NoItemAtLocationException($"Tried to get an item at position {index}, " +
                    $"but no item exists there");
            }
            else
            {
                array[index] = default;
                return item;
            }
        }
    }
}
